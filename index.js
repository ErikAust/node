const express = require('express');
const app = express();
const fs = require('fs');
const morgan = require('morgan');

const filePattern = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/);

app.use(express.json());
app.use(morgan('tiny'));

app.post('/api/files', (req, res) => {
    if (!filePattern.test(req.body.filename)) {
        res.status(400).json({ 'message': `Incorrect filename format` });
    } else if (typeof req.body.content === 'undefined') {
        res.status(400).json({ 'message': `Please specify 'content' parameter` });
    }
    fs.writeFile(`./api/files/${req.body.filename}`, req.body.content, (err) => {
        if (err) {
            console.log(err);
        }
    })
    res.status(200).json({ 'message': 'File created successfully' });
});

app.get('/api/files', (req, res) => {
    fs.readdir('./api/files', (err, files) => {
        if (err) {
            console.log(err);
        } else if (files.length === 0) {
            res.status(400).json({ 'message': `Client error` });
        }
        res.status(200).json({ 'message': 'Success', 'files': [...files] });
    });
});

app.get(`/api/files/:filename`, (req, res) => {
    fs.stat(`./api/files/${req.params.filename}`, (err, stat) => {
        if (err) {
            res.status(400).json({ 'message': `No file with '${req.params.filename}' filename found` });
        } else {
            const ext = req.params.filename.split('.');
            fs.readFile(`./api/files/${req.params.filename}`, 'utf8', (err, data) => {
                if (err) {
                    console.log(err);
                }
                res.status(200).json({
                    'message': 'Success',
                    'filename': req.params.filename,
                    'content': data,
                    'extension': ext[ext.length - 1],
                    'uploadedDate': stat.birthtime
                });
            });
        };
    });
});

app.use((err, req, res, next) => {
    res.status(500).json({ 'message': 'Server error' });
});

app.listen(8080);